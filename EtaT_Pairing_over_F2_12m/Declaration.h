#pragma once

// test
void testStandard();
void testLogicalOp();
void testField();
void testF2_511();
void test_setString();
void test_shiftBit();
void testMul();
void testMult511Cmp();
void testMult487Cmp();
void testSqBF_Cmp();
void testExtField_basic();
void testMultCmp_ExtField();
void testMultCmp_ExtField_487();
void testMultCmpLoadShift();

// timing
void timingF12_6_2();
void timingPairing511();
void timingPairing487();
void testExtField3_2_2_basic();