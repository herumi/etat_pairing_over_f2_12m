#include <iostream>
#include <string>
#include <vector>
#include <stdint.h>

#include "Declaration.h"
#include "F2_487.h"
#include "F2_511.h"
#include "extension_field6_2.h"
#include "extension_field3_2_2.h"
#include "pairing.h"

int main(int arg, char **argv)
{
	// test
	//testStandard();
	//testLogicalOp();
	//testField();
	//testF2_511();
	//test_setString();
	//test_shiftBit();

	//testMul();
	//testMult511Cmp();
	//testMult487Cmp();
	//testSqBF_Cmp();

	//testExtField_basic();
	//testExtField3_2_2_basic();
	//testMultCmpLoadShift();
	//testMultCmp_ExtField();
	//testMultCmp_ExtField_487();

	// timing
	//timingF12_6_2();
	//timingPairing511();
    timingPairing487();

#if 0

	F2_511_r128 F, G, Res;
	F.setRandom();
	G.setRandom();
	const int count = 100000;

	for (int i = 0; i < count; ++i) {
		F2_511_r128::mulCombL2RwithWindow<6>(Res, F, G);
	}

	for (int i = 0; i < count; ++i) {
		F2_511_r128::mulCombR2L_RegSize(Res, F, G);
	}

#endif
	return 0;
}