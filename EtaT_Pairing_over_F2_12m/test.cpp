#include <iostream>
#include <stdint.h>

#include "F2_511.h"
#include "F2_487.h"
#include "extension_field6_2.h"
#include "extension_field3_2_2.h"

void testStandard() {

	int size = 12;
	for (int i = 0; i < size; ++i) {
		for (int j = i + 1; j < size; ++j) {

			std::cout << address_st(i, j, size) << std::endl;
		}
	}
}

void testLogicalOp() {
	char2_128 a, b, c, d;

	a.clear();
	c = a & b;
	c = a | b;

	c &= a;
	b ^= b;

	a == b;
	a == d;

	a.clear();
	a.setBit1(70);
	a.setBit1(70);

	F2_511_r128 a511;
	a511.clear();
	a511.setBit1(510);
}

void testField() {

	F2_BaseField<char2_128, 129> F, G, H;
	F2_BaseField<char2_128, 129>::add(H, F, G);

	char2_128 a, b;
	b.clear();

	F.xReg[0] = a;
	F.xReg[1] = b;

	G.xReg[0] = b;
	G.xReg[1] = a;

	H = F & G;
	H |= F;
	H ^= H;

	H.setRandom();

	uint32_t a32 = 0;
	b.setRandom();
	for (unsigned int i = 0; i < 16; ++i) a32 += b.getBit(i);

	std::string str, str2;
	str = "000000000131426345764587856765787897657864635453430240850";
	formatStr(str);

	str = b.getStringAll();
	str2 = b.getString();

	str = H.getString();
}

void testF2_511() {

	char2_128 test;
	test.setRandom();
	test.testZero();

	F2_511_r128 A, B, C;
	B.clear();
	A.testZero();
	B.testZero();

	F2_BaseField<char2_128, 256> D256;
	F2_BaseField<char2_128, 192> D192;
	F2_BaseField<char2_128, 119> D119;

	D256.clearUpDeg();
	D119.clearUpDeg();
	D192.clearUpDeg();

	A += B;
	F2_511_r128::add(C, A, B);

	B.clearUpDeg();

	A.clear();
	A.setRandom();

	A.clear();
	B.clear();
	B.xReg[3].setString("1");
	A == B;
}

void test_setString() {

	uint32_t a[4];
	const char str[128] = "1 0001 0010 0011 0100 0101 0110 0111 1000 1001 1111";
	const char str160[1024] = "10010001101000101011001111000 10010000000100100011010001010110 01111000100100000001001000110100 01010110011110001001000000010010 00110100010101100111100010010000";

	setString32<4>(a, str);

	char2_128 A;
	A.setString(str);

	F2_BaseField<char2_128, 192> F;
	F.setString(str);

	F.setString(str160);

	F2_511_r128 P;
	P.setString(str160);

	std::string strStore, strStore2;
	strStore = P.getString();
	strStore2 = P.getString_R();
}

void test_shiftBit() {

	char2_128 a, b;

	a.shiftLeftBit<65>();
	b.shiftRightBit<126>();
	a = b;

	a = psllq<3>(a);
	b = psrlq<3>(b);

	a = pslldq<3>(a);
	b = psrldq<5>(b);

	F2_BaseField<char2_128, 511> F, G, H;

	F.setRandom();
	G = F;
	F.shiftLeftBit<440>();
	G.shiftLeftByte<55>();
	F == G;

	H = F2_BaseField<char2_128, 511>::shiftRightBit<7>(G);
	G.shiftRightBit<7>();

	F = F2_BaseField<char2_128, 511>::shiftLeftRegSize(G, 1);
	G.shiftRightRegSize(3);

	F2_511_r128 P, Q;
	P.shiftLeftBit<258>();
	P = F2_511_r128::shiftLeftBit<258>(Q);

	uint32_t a32[16];
	P.copyTo32(a32);
	P.copyReg(Q);
}

void testMul() {

	F2_511_r128 F, G, X313, Res, Res2, Res3, Res4;
	F2_487_r128 F_487, G_487, X213_487, Res_487, Res2_487;
	uint32_t a[16], b[16];

	for (int i = 0; i < 16; ++i) a[i] = 0;
	a[0] = 2;

	// x^510
	for (int i = 0; i < 16; ++i) b[i] = 0;
	b[15] = 0x40000000;

	/*
	x^1020 = x^509 + x^18 + x^8
	*/


	F.clearUpDeg();

	F.loadFrom32(b);
	G.loadFrom32(b);

	F_487.setRandom();
	G_487.setRandom();

	F2_487_r128::mulShAdd(Res_487, F_487, G_487);
	//F2_487_r128::mulCombR2L_RegSize(Res2_487, F_487, G_487);
	F2_487_r128::mulCombL2RwithWindow<5>(Res2_487, F_487, G_487);
	Res_487 == Res2_487;

	F2_487_r128::mulShAdd(Res_487, F_487, F_487);
	F2_487_r128::square(Res2_487, F_487);
	Res_487 == Res2_487;

	X213_487.clear();
	X213_487.setBit1(213);
	F2_487_r128::mulShAdd(Res_487, F_487, X213_487);
	F2_487_r128::mulMonomial<213>(Res2_487, F_487);
	Res_487 == Res2_487;



	F.setRandom();
	G.setRandom();
	F2_511_r128::mulShAdd(Res, F, G);
	F2_511_r128::mulCombL2RwithWindow<6>(Res4, F, G);
	Res == Res4;

	F2_511_r128::mulCombR2L_RegSize(Res2, F, G);
	F2_511_r128::mulCombR2L<64>(Res3, F, G);

	F.setRandom();
	X313.clear();
	X313.setBit1(313);
	F2_511_r128::mulShAdd(Res, F, X313);
	F2_511_r128::mulMonomial<313>(Res2, F);
	Res == Res2;

	F2_511_r128::mulShAdd(Res, F, F);
	F2_511_r128::square(Res2, F);
	Res == Res2;
}

void testMult511Cmp(){

	const int Count= 5;
	bool cmp, cmp2;
	int correct, error;
	std::string strShAdd, strRegSize, strComb;
	F2_511_r128 A, B, C_ShAdd, C_RegSize, C_Comb;

	std::cout << "F2^511 mul test" << std::endl;

	correct = 0;
	error = 0;
	for (unsigned int i = 0; i < Count; ++i) {

		A.setRandom();
		A.clearUpDeg();

		B.setRandom();
		B.clearUpDeg();

		F2_511_r128::mulShAdd(C_ShAdd, A, B);
		F2_511_r128::mulCombR2L_RegSize(C_RegSize, A, B);
		F2_511_r128::mulLoadShift8(C_Comb, A, B);

		strShAdd = C_ShAdd.getString();
		strRegSize = C_RegSize.getString();
		strComb = C_Comb.getString();

		cmp = C_ShAdd == C_RegSize;
		cmp2 = C_ShAdd == C_Comb;

		if (!cmp || !cmp2) {

			++error;
			if (cmp2) std::cout << "mul (shift add) :\n" << strShAdd << std::endl << "neq\n" << "mul (Comb: RegSize) :\n" << strRegSize << std::endl;
			else if (cmp) std::cout << "mul (shift add):\n" << strShAdd << std::endl << "neq\n" << "mul (load shift)\n" << strComb << std::endl;
			else std::cout << "mul (shift add):\n" << strShAdd << std::endl << "neq\n" << "mul (Comb: RegSize) :\n" << strRegSize << ", \n" << "mul (load shift) :\n" << strComb << std::endl;
		}

		else{

			++correct;
			std::cout << "mul (shift add) :\n" << strShAdd << std::endl << "==\n" << "mul (Comb: RegSize) :\n" << strRegSize << ", \n" << "mul (load shift) :\n" << strComb << std::endl;
		}
	}

	std::cout << "correct: " << correct << " (/" << Count << ")\n";
	std::cout << "error: " << error << " (/" << Count << ")\n";
}

void testMult487Cmp(){

	const int Count= 5;
	bool cmp, cmp2;
	int correct, error;
	std::string strShAdd, strRegSize, strLoad;
	F2_487_r128 A, B, C_ShAdd, C_RegSize, C_Load;

	std::cout << "F2^487 mul test" << std::endl;

	correct = 0;
	error = 0;
	for (unsigned int i = 0; i < Count; ++i) {

		A.setRandom();
		A.clearUpDeg();

		B.setRandom();
		B.clearUpDeg();

		F2_487_r128::mulShAdd(C_ShAdd, A, B);
		F2_487_r128::mulCombR2L_RegSize(C_RegSize, A, B);
		F2_487_r128::mulLoadShift8(C_Load, A, B);

		strShAdd = C_ShAdd.getString();
		strRegSize = C_RegSize.getString();
		strLoad = C_Load.getString();

		cmp = C_ShAdd == C_RegSize;
		cmp2 = C_ShAdd == C_Load;

		if (!cmp || !cmp2) {

			++error;
			if (cmp2) std::cout << "mul (shift add) :\n" << strShAdd << std::endl << "neq\n" << "mul (Comb: RegSize) :\n" << strRegSize << std::endl;
			else if (cmp) std::cout << "mul (shift add):\n" << strShAdd << std::endl << "neq\n" << "mul (load shift)\n" << strLoad << std::endl;
			else std::cout << "mul (shift add):\n" << strShAdd << std::endl << "neq\n" << "mul (Comb: RegSize) :\n" << strRegSize << ", \n" << "mul (load shift) :\n" << strLoad << std::endl;
		}

		else{

			++correct;
			std::cout << "mul (shift add) :\n" << strShAdd << std::endl << "==\n" << "mul (Comb: RegSize) :\n" << strRegSize << ", \n" << "mul (load shift) :\n" << strLoad << std::endl;
		}
	}

	std::cout << "correct: " << correct << " (/" << Count << ")\n";
	std::cout << "error: " << error << " (/" << Count << ")\n";
}

void testSqBF_Cmp() {

	const int Count= 5;
	bool cmp;
	int correct, error;
	std::string strShAdd, strSq;
	F2_511_r128 A, C_ShAdd, C_sq;

	correct = 0;
	error = 0;
	for (unsigned int i = 0; i < Count; ++i) {

		A.setRandom();
		A.clearUpDeg();

		F2_511_r128::mulShAdd(C_ShAdd, A, A);
		F2_511_r128::square(C_sq, A);

		strShAdd = C_ShAdd.getString();
		strSq = C_sq.getString();

		cmp = C_ShAdd == C_sq;

		if (!cmp) {

			++error;
			std::cout << "mul (shift add) :\n" << strShAdd << std::endl << "neq\n" << "square :\n" << strSq << std::endl;
		}
		else{

			++correct;
			std::cout << "mul (shift add) :\n" << strShAdd << std::endl << "==\n" << "square :\n" << strSq << std::endl;
		}
	}

	std::cout << "correct: " << correct << " (/" << Count << ")\n";
	std::cout << "error: " << error << " (/" << Count << ")\n";
}

void testExtField_basic() {

	F2_ExtField6<F2_511_r128> A, B, C, C_reg, C_Comb, C_sq;
	F2_ExtField_Double<F2_ExtField6<F2_511_r128>> D;

	A.reduction(D);

	A.clear();
	B.clear();

	// A = w
	A.xBF[1].setString("1");

	// B = w^5
	B.xBF[5].setString("1");

	F2_ExtField6<F2_511_r128>::mulShAdd(C, A, B);
	F2_ExtField6<F2_511_r128>::mulCombR2L_RegSize(C_reg, A, B);
	F2_ExtField6<F2_511_r128>::mulCombR2L<64>(C_Comb, A, B);

	C == C_reg;
	C == C_Comb;

	A.setRandom();
	F2_ExtField6<F2_511_r128>::mulShAdd(C, A, A);
	F2_ExtField6<F2_511_r128>::square(C_sq, A);

	C == C_sq;



	F2_ExtField6_2<F2_511_r128> A12, B12, C12, C12_sq;
	A12.clear();
	B12.clear();
	A12.xF6[1].xBF[0].setString("1");
	B12.xF6[1].xBF[0].setString("1");

	F2_ExtField6_2<F2_511_r128>::mulShAdd(C12, A12, B12);

	A12.setRandom();
	F2_ExtField6_2<F2_511_r128>::mulShAdd(C12, A12, A12);
	F2_ExtField6_2<F2_511_r128>::square(C12_sq, A12);

	C12 == C12_sq;


	F2_ExtField6<F2_511_r128> alpha, beta;
	alpha.clear();
	alpha.xBF[0].setBit1(0);
	beta.clearUpDeg();
	beta.xBF[3].clear();
	beta.xBF[5].clear();

	F2_ExtField6_2<F2_511_r128>::alpha_beta(C12, alpha, beta);
}

void testExtField3_2_2_basic() {

	F2_ExtField3<F2_511_r128> A3, B3, C3, C3_reg, C3_Comb;
	A3.clear();
	B3.clear();

	A3.plusOne();

	F2_ExtField3<F2_511_r128>::mulShAdd(C3, A3, B3);
	F2_ExtField3<F2_511_r128>::mulCombR2L_RegSize(C3_reg, A3, B3);



	F2_ExtField3_2_2<F2_511_r128> A, B, C, C_reg, C_Comb, C_sq;
	F2_ExtField_Double<F2_ExtField3_2_2<F2_511_r128>> D;

	A.setRandom();
	B.setRandom();

	F2_ExtField3_2_2<F2_511_r128>::mulShAdd(C, A, B);
	F2_ExtField3_2_2<F2_511_r128>::mulCombR2L_RegSize(C_reg, A, B);
	F2_ExtField3_2_2<F2_511_r128>::mulCombR2L<64>(C_Comb, A, B);

	C == C_reg;
	C == C_Comb;

	A.setRandom();
	F2_ExtField3_2_2<F2_511_r128>::mulShAdd(C, A, A);
	F2_ExtField3_2_2<F2_511_r128>::square(C_sq, A);

	C == C_sq;
}

void testMultCmp_ExtField(){

	const int Count= 5;
	bool cmp, cmp2;
	int correct, error, correct2, error2, correct3, error3;
	F2_ExtField6_2<F2_511_r128> A, B, C_ShAdd, C_RegSize, C_Comb;
	F2_ExtField3_2_2<F2_511_r128> A2, B2, C_ShAdd2, C_RegSize2, C_Comb2;
	F2_ExtField3<F2_511_r128> A3, B3, C_ShAdd3, C_RegSize3, C_Comb3;

	correct = 0;
	error = 0;
	correct2 = 0;
	error2 = 0;
	correct3 = 0;
	error3 = 0;

	std::cout << "test for multiplication on F2Ext6_2" << std::endl;
	for (unsigned int i = 0; i < Count; ++i) {

		A.setRandom();
		B.setRandom();

		F2_ExtField6_2<F2_511_r128>::mulShAdd(C_ShAdd, A, B);
		F2_ExtField6_2<F2_511_r128>::mulCombR2L_RegSize(C_RegSize, A, B);
		F2_ExtField6_2<F2_511_r128>::mulLoadShift8(C_Comb, A, B);

		cmp = C_ShAdd == C_RegSize;
		cmp2 = C_ShAdd == C_Comb;

		if (!cmp || !cmp2) {

			++error;
			if (cmp2) std::cout << "mul (shift add) __neq__ mul (Comb: RegSize)" << std::endl;
			else if (cmp) std::cout << "mul (shift add) __neq__ mul (load shift)" << std::endl;
			else std::cout << "mul (shift add) __neq__ mul (Comb: RegSize), mul (load shift)" << std::endl;
		}

		else{

			++correct;
			std::cout << "mul (shift add) == mul (Comb: RegSize), mul (load shift)" << std::endl;
		}
	}

	std::cout << "test for multiplication on F2Ext3" << std::endl;
	for (unsigned int i = 0; i < Count; ++i) {

		A3.setRandom();
		B3.setRandom();

		F2_ExtField3<F2_511_r128>::mulShAdd(C_ShAdd3, A3, B3);
		F2_ExtField3<F2_511_r128>::mulCombR2L_RegSize(C_RegSize3, A3, B3);
		F2_ExtField3<F2_511_r128>::mulLoadShift8(C_Comb3, A3, B3);

		cmp = C_ShAdd3 == C_RegSize3;
		cmp2 = C_ShAdd3 == C_Comb3;

		if (!cmp || !cmp2) {

			++error3;
			if (cmp2) std::cout << "mul (shift add) __neq__ mul (Comb: RegSize)" << std::endl;
			else if (cmp) std::cout << "mul (shift add) __neq__ mul (load shift)" << std::endl;
			else std::cout << "mul (shift add) __neq__ mul (Comb: RegSize), mul (load shift)" << std::endl;
		}

		else{

			++correct3;
			std::cout << "mul (shift add) == mul (Comb: RegSize), mul (load shift)" << std::endl;
		}
	}

	std::cout << "test for multiplication on F2Ext3_2_2" << std::endl;
	for (unsigned int i = 0; i < Count; ++i) {

		A2.setRandom();
		B2.setRandom();

		F2_ExtField3_2_2<F2_511_r128>::mulShAdd(C_ShAdd2, A2, B2);
		F2_ExtField3_2_2<F2_511_r128>::mulCombR2L_RegSize(C_RegSize2, A2, B2);
		F2_ExtField3_2_2<F2_511_r128>::mulLoadShift8(C_Comb2, A2, B2);

		cmp = C_ShAdd2 == C_RegSize2;
		cmp2 = C_ShAdd2 == C_Comb2;

		if (!cmp || !cmp2) {

			++error2;
			if (cmp2) std::cout << "mul (shift add) __neq__ mul (Comb: RegSize)" << std::endl;
			else if (cmp) std::cout << "mul (shift add) __neq__ mul (load shift)" << std::endl;
			else std::cout << "mul (shift add) __neq__ mul (Comb: RegSize), mul (load shift)" << std::endl;
		}

		else{

			++correct2;
			std::cout << "mul (shift add) == mul (Comb: RegSize), mul (load shift)" << std::endl;
		}
	}

	std::cout << "mult. on extensionfield test" << std::endl;

	std::cout << "F6_2: " << std::endl;
	std::cout << "correct: " << correct << " (/" << Count << ")\n";
	std::cout << "error: " << error << " (/" << Count << ")\n";

	std::cout << "F3: " << std::endl;
	std::cout << "correct: " << correct3 << " (/" << Count << ")\n";
	std::cout << "error: " << error3 << " (/" << Count << ")\n";

	std::cout << "F3_2_2: " << std::endl;
	std::cout << "correct: " << correct2 << " (/" << Count << ")\n";
	std::cout << "error: " << error2 << " (/" << Count << ")\n";
}

void testMultCmp_ExtField_487(){

	const int Count= 5;
	bool cmp, cmp2;
	int correct, error, correct2, error2, correct3, error3;
	F2_ExtField6_2<F2_487_r128> A, B, C_ShAdd, C_RegSize, C_Comb;
	F2_ExtField3_2_2<F2_487_r128> A2, B2, C_ShAdd2, C_RegSize2, C_Comb2;
	F2_ExtField3<F2_487_r128> A3, B3, C_ShAdd3, C_RegSize3, C_Comb3;

	correct = 0;
	error = 0;
	correct2 = 0;
	error2 = 0;
	correct3 = 0;
	error3 = 0;

	std::cout << "test for multiplication on F2^487 Ext6_2" << std::endl;
	for (unsigned int i = 0; i < Count; ++i) {

		A.setRandom();
		B.setRandom();

		F2_ExtField6_2<F2_487_r128>::mulShAdd(C_ShAdd, A, B);
		F2_ExtField6_2<F2_487_r128>::mulCombR2L_RegSize(C_RegSize, A, B);
		F2_ExtField6_2<F2_487_r128>::mulLoadShift8(C_Comb, A, B);

		cmp = C_ShAdd == C_RegSize;
		cmp2 = C_ShAdd == C_Comb;

		if (!cmp || !cmp2) {

			++error;
			if (cmp2) std::cout << "mul (shift add) __neq__ mul (Comb: RegSize)" << std::endl;
			else if (cmp) std::cout << "mul (shift add) __neq__ mul (load shift)" << std::endl;
			else std::cout << "mul (shift add) __neq__ mul (Comb: RegSize), mul (load shift)" << std::endl;
		}

		else{

			++correct;
			std::cout << "mul (shift add) == mul (Comb: RegSize), mul (load shift)" << std::endl;
		}
	}

	std::cout << "test for multiplication on F2^487 Ext3" << std::endl;
	for (unsigned int i = 0; i < Count; ++i) {

		A3.setRandom();
		B3.setRandom();

		F2_ExtField3<F2_487_r128>::mulShAdd(C_ShAdd3, A3, B3);
		F2_ExtField3<F2_487_r128>::mulCombR2L_RegSize(C_RegSize3, A3, B3);
		F2_ExtField3<F2_487_r128>::mulLoadShift8(C_Comb3, A3, B3);

		cmp = C_ShAdd3 == C_RegSize3;
		cmp2 = C_ShAdd3 == C_Comb3;

		if (!cmp || !cmp2) {

			++error3;
			if (cmp2) std::cout << "mul (shift add) __neq__ mul (Comb: RegSize)" << std::endl;
			else if (cmp) std::cout << "mul (shift add) __neq__ mul (load shift)" << std::endl;
			else std::cout << "mul (shift add) __neq__ mul (Comb: RegSize), mul (load shift)" << std::endl;
		}

		else{

			++correct3;
			std::cout << "mul (shift add) == mul (Comb: RegSize), mul (load shift)" << std::endl;
		}
	}

	std::cout << "test for multiplication on F2^487 Ext3_2_2" << std::endl;
	for (unsigned int i = 0; i < Count; ++i) {

		A2.setRandom();
		B2.setRandom();

		F2_ExtField3_2_2<F2_487_r128>::mulShAdd(C_ShAdd2, A2, B2);
		F2_ExtField3_2_2<F2_487_r128>::mulCombR2L_RegSize(C_RegSize2, A2, B2);
		F2_ExtField3_2_2<F2_487_r128>::mulLoadShift8(C_Comb2, A2, B2);

		cmp = C_ShAdd2 == C_RegSize2;
		cmp2 = C_ShAdd2 == C_Comb2;

		if (!cmp || !cmp2) {

			++error2;
			if (cmp2) std::cout << "mul (shift add) __neq__ mul (Comb: RegSize)" << std::endl;
			else if (cmp) std::cout << "mul (shift add) __neq__ mul (load shift)" << std::endl;
			else std::cout << "mul (shift add) __neq__ mul (Comb: RegSize), mul (load shift)" << std::endl;
		}

		else{

			++correct2;
			std::cout << "mul (shift add) == mul (Comb: RegSize), mul (load shift)" << std::endl;
		}
	}

	std::cout << "mult. on extensionfield test" << std::endl;

	std::cout << "F6_2 (m=487): " << std::endl;
	std::cout << "correct: " << correct << " (/" << Count << ")\n";
	std::cout << "error: " << error << " (/" << Count << ")\n";

	std::cout << "F3 (m=487): " << std::endl;
	std::cout << "correct: " << correct3 << " (/" << Count << ")\n";
	std::cout << "error: " << error3 << " (/" << Count << ")\n";

	std::cout << "F3_2_2 (m=487): " << std::endl;
	std::cout << "correct: " << correct2 << " (/" << Count << ")\n";
	std::cout << "error: " << error2 << " (/" << Count << ")\n";
}

void testMultCmpLoadShift() {

	F2_487_r128 A, B, C8, C64;
	A.setRandom();
	B.setRandom();

	F2_487_r128::mulLoadShift8(C8, A, B);
	//F2_487_r128::mulLoadShift64(C64, A, B);

	if (C8 == C64) std::cout << "m=487, load shift 8 -- load shift 64: O.K." << std::endl;
	else std::cout << "m=487, load shift 8 -- load shift 64: NG" << std::endl;
}