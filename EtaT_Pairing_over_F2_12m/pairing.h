#pragma once

#include <stdint.h>
#include <vector>

#include "F2_511.h"
#include "F2_487.h"
#include "extension_field6_2.h"
#include "extension_field3_2_2.h"



template<class BaseField>
F2_ExtField6_2<BaseField> etaT_pairing6_2(const BaseField& xp0, const BaseField& xq0, const BaseField& yp0, const BaseField& yq0) {

	static const int degBF = BaseField::ext_degree;
	static const int nFull = F2_ExtField6_2<BaseField>::nFull;

	F2_ExtField6_2<BaseField> pairingValue, alphaBeta, tmp6_2;
	pairingValue.clear();
	pairingValue.plusOne();

	F2_ExtField6<BaseField> alpha, beta, b4_F6;
	alpha.clear();
	beta.clear();
	b4_F6.clear();

	BaseField xpBF, ypBF, tmpBF_1;
	std::vector<AlignArray32<BaseField>> xP(degBF), xQ(degBF), yP(degBF), yQ(degBF);

	int k1, k2, k3, k4, k5, k6;

	// precomputation
	xP[0].store(xp0);
	xQ[0].store(xq0);
	yP[0].store(yp0);
	yQ[0].store(yq0);

	for (int i = 1; i < degBF; ++i) {

		AlignArray32<BaseField>::square(xP[i], xP[i - 1]);
		AlignArray32<BaseField>::square(xQ[i], xQ[i - 1]);
		AlignArray32<BaseField>::square(yP[i], yP[i - 1]);
		AlignArray32<BaseField>::square(yQ[i], yQ[i - 1]);
	}

	// Miller loop
	for (int i = 0; i < (degBF - 3) / 2; ++i) {


		k1 = ((3 * degBF - 9 - 6 * i) / 2) % degBF;
		k2 = (k1 + 1) % degBF;
		k3 = (k2 + 1) % degBF;
		k4 = ((3 * degBF - 3 - 6 * i) / 2) % degBF;
		k5 = (k4 + 1) % degBF;
		k6 = (k5 + 1) % degBF;

		// compute \alpha = (alpha,1)
		// alpha = a0 + a1w + a2w^2 + a4w^4

		// a4 = xP[k4] + xP[k5]
		(xP[k4] + xP[k5]).load(alpha.xBF[4]);

		// a2 = xP[k4] + 1 + xQ[k3]
		(xP[k4] + xQ[k3]).load(alpha.xBF[2]);
		alpha.xBF[2].plusOne();

		// a1 = xQ[k2] + xQ[k3]
		(xQ[k2] + xQ[k3]).load(alpha.xBF[1]);

		//a0 = yQ[k2] + a2xQ[k2] + a4xQ[k3] + yP[k4]
		BaseField::mulCombR2L_RegSize(tmpBF_1, alpha.xBF[2], xQ[k2].retBF());
		alpha.xBF[0] = yQ[k2].retBF() + tmpBF_1;
		BaseField::mulCombR2L_RegSize(tmpBF_1, alpha.xBF[4], xQ[k3].retBF());
		alpha.xBF[0] += tmpBF_1;
		alpha.xBF[0] += yP[k4].retBF();


		// compute \beta = (beta,1)
		// beta = b0 + b1w + b2w^2 + b4w^4

		// b1 = xP[k5] + xP[k6]
		(xP[k5] + xP[k6]).load(beta.xBF[1]);

		// b2 = xP[k6] + xQ[k1] + 1
		(xP[k6] + xQ[k1]).load(beta.xBF[2]);
		beta.xBF[2].plusOne();

		// b4 = xQ[k1] + xQ[k2] + 1
		(xQ[k1] + xQ[k2]).load(beta.xBF[4]);
		beta.xBF[4].plusOne();

		// b0 = yQ[k1] + b1xQ[k1] + yP[k5] + xP[k6](xP[k5] + xQ[k2]) + xP[k5]
		BaseField::mulCombR2L_RegSize(tmpBF_1, beta.xBF[1], xQ[k1].retBF());
		beta.xBF[0] = yQ[k1].retBF() + tmpBF_1;
		beta.xBF[0] += yP[k5].retBF();
		beta.xBF[0] += xP[k5].retBF();
		BaseField::mulCombR2L_RegSize( tmpBF_1, xP[k6].retBF(), xP[k5].retBF() + xQ[k2].retBF() );
		beta.xBF[0] += tmpBF_1;

		// \alpha\beta
		F2_ExtField6_2<BaseField>::alpha_beta(alphaBeta, alpha, beta);

		F2_ExtField6_2<BaseField>::mulCombR2L_RegSize(tmp6_2, pairingValue, alphaBeta);
	    pairingValue.copyReg(tmp6_2);

	}

	// extract current point (xP,yP)
	xpBF = xP[degBF - 3].retBF();
	xpBF.plusOne();
	ypBF = yP[degBF - 3].retBF() + xP[degBF - 2].retBF();

	// final doublings / addition
	/*
	b4_F6 = (yQ + xQ[1](1 + xQ + xpBF^8 + xpBF^4) + xpBF^4xQ + ypBF^4) +
	        (xQ[1] + xpBF^4)w + (xpBF^8 + xpBF^4)w^2 + w^3 + (xQ[1] + xQ)w^4
	*/

	// b4_3
	b4_F6.xBF[3].plusOne();

	// b4_4
	b4_F6.xBF[4] = xQ[1].retBF() + xq0;

	// xpBF := xpBF^4
	BaseField::square(tmpBF_1, xpBF);
	BaseField::square(xpBF, tmpBF_1);

	// b4_1
	b4_F6.xBF[1] = xQ[1].retBF() + tmpBF_1;

	// tmpBF_1 := xpBF^8
	BaseField::square(tmpBF_1, xpBF);

	// b4_2;
	b4_F6.xBF[2] = tmpBF_1 + xpBF;

	// b4_0
	// tmpBF_1 := b4_2 + xq0
	tmpBF_1 = b4_F6.xBF[2] + xq0;
	tmpBF_1.plusOne();
	BaseField::mulCombR2L_RegSize(b4_F6.xBF[0],xQ[1].retBF(),tmpBF_1);

	//tmpBF_1 = xpBF^4xq0
	BaseField::mulCombR2L_RegSize(tmpBF_1, xpBF, xq0);
	b4_F6.xBF[0] += tmpBF_1;
	b4_F6.xBF[0] += yq0;

	// ypBF := ypBF^4
	BaseField::square(tmpBF_1, ypBF);
	BaseField::square(ypBF, tmpBF_1);
	b4_F6.xBF[0] += ypBF;

	// f^4
	F2_ExtField6_2<BaseField>::square(tmp6_2, pairingValue);
	F2_ExtField6_2<BaseField>::square(pairingValue, tmp6_2);

	// f^4(y+b4(x))
	F2_ExtField6_2<BaseField>::sparseMul(tmp6_2, pairingValue, b4_F6);
	pairingValue.copyReg(tmp6_2);


	// final exponentiation
	/* !! perform only (degBF+1)/2 squarings !! */

	for (int i = 0; i < (degBF + 1) / 2; ++i) {

		F2_ExtField6_2<BaseField>::square(tmp6_2, pairingValue);
		pairingValue.copyReg(tmp6_2);
	}

	return pairingValue;
}



// 3-2-2
/*\gamma_1������*/
template<class BaseField>
F2_ExtField3_2_2<BaseField> etaT_pairing3_2_2(const BaseField& xp0, const BaseField& xq0, const BaseField& yp0, const BaseField& yq0) {

	static const int degBF = BaseField::ext_degree;
	static const int nFull = F2_ExtField6_2<BaseField>::nFull;

	F2_ExtField3_2_2<BaseField> pairingValue, alphaBeta, tmp3_2_2;
	pairingValue.clear();
	pairingValue.plusOne();

	F2_ExtField3_2<BaseField> alpha, beta, b4_F6;
	alpha.clear();
	beta.clear();
	b4_F6.clear();

	BaseField xpBF, ypBF, d, tmpBF_1, tmpBF_2;
	std::vector<AlignArray32<BaseField>> xP(degBF), xQ(degBF), yP(degBF), yQ(degBF);

	int k1, k2, k3, k4, k5, k6;

	// precomputation
	xP[0].store(xp0);
	xQ[0].store(xq0);
	yP[0].store(yp0);
	yQ[0].store(yq0);


	for (int i = 1; i < degBF; ++i) {

		AlignArray32<BaseField>::square(xP[i], xP[i - 1]);
		AlignArray32<BaseField>::square(xQ[i], xQ[i - 1]);
		AlignArray32<BaseField>::square(yP[i], yP[i - 1]);
		AlignArray32<BaseField>::square(yQ[i], yQ[i - 1]);
	}

	// Miller loop
	for (int i = 0; i < (degBF - 3) / 2; ++i) {


		k1 = ((3 * degBF - 9 - 6 * i) / 2) % degBF;
		k2 = (k1 + 1) % degBF;
		k3 = (k2 + 1) % degBF;
		k4 = ((3 * degBF - 3 - 6 * i) / 2) % degBF;
		k5 = (k4 + 1) % degBF;
		k6 = (k5 + 1) % degBF;

		// compute \alpha = (alpha,1)
		// alpha = a0 + a1w + a2w^2 + a3s (a3=c)

		// a2 = xP[k4] + xP[k5]
		(xP[k4] + xP[k5]).load(alpha.xF3[0].xBF[2]);

		// a3 = xQ[k2] + xP[k5] + 1
		(xQ[k2] + xP[k5]).load(alpha.xF3[0].xBF[1]);

		// !!! use a3+1 for a0 !!!
		BaseField::mulCombR2L_RegSize(tmpBF_1, alpha.xF3[0].xBF[1], xQ[k3].retBF()); //tmpBF_1 = (a3 + 1)xQ[k3]
		alpha.xF3[0].plusOne();

		// a1 = xQ[k3] + xP[k5] + 1 + \gamma_1
		(xP[k5] + xQ[k3]).load(alpha.xF3[0].xBF[1]);
		alpha.xF3[0].xBF[1].plusOne();

		//a0 = yQ[k2] + (a3 + 1)xQ[k3] + (xP[k4] + 1 + xQ[k3])xQ[k2] + yP[k4] + xP[k4] + \gamma_1
		alpha.xF3[0].xBF[0] = yQ[k2].retBF() + tmpBF_1;
		alpha.xF3[0].xBF[0] += xP[k4].retBF();
		alpha.xF3[0].xBF[0] += yP[k4].retBF();
		tmpBF_1 = xP[k4].retBF() + xQ[k3].retBF();
		tmpBF_1.plusOne();
		BaseField::mulCombR2L_RegSize(tmpBF_2, tmpBF_1, xQ[k2].retBF());
		alpha.xF3[0].xBF[0] += tmpBF_2;



		// compute \beta = (beta,1)
		// beta = b0 + b1w + b2w^2 + b3s (a3=b3=c)

		// b1 = xQ[k2] + xP[k6] + 1 + \gamma_1
		(xQ[k2] + xP[k6]).load(beta.xF3[0].xBF[1]);
		beta.xF3[0].xBF[1].plusOne();

		// b2 = xQ[k2] + xQ[k1] + \tgamma_1
		(xQ[k2] + xQ[k1]).load(beta.xF3[0].xBF[2]);
	
		// b0 = yQ[k1] + yP[k5] + xQ[k1](xP[k5] + xP[k6] + 1) + a3*xP[k6] + xP[k5]+ \gamma_1
		tmpBF_1 = xP[k6].retBF() + xP[k5].retBF();
		tmpBF_1.plusOne();
		BaseField::mulCombR2L_RegSize(beta.xF3[0].xBF[0], tmpBF_1, xQ[k1].retBF());
		beta.xF3[0].xBF[0] += yQ[k1].retBF();
		beta.xF3[0].xBF[0] += yP[k5].retBF();
		beta.xF3[0].xBF[0] += xP[k5].retBF();
		BaseField::mulCombR2L_RegSize(tmpBF_1, xP[k6].retBF(), alpha.xF3[1].xBF[0]);		
		beta.xF3[0].xBF[0] += tmpBF_1;



		// \alpha\beta
		F2_ExtField3_2_2<BaseField>::alpha_beta(alphaBeta, alpha, beta, d);

		F2_ExtField3_2_2<BaseField>::mulCombR2L_RegSize(tmp3_2_2, pairingValue, alphaBeta);
		pairingValue.copyReg(tmp3_2_2);

	}

	// extract current point (xP,yP)
	xpBF = xP[degBF - 3].retBF();
	xpBF.plusOne();
	ypBF = yP[degBF - 3].retBF() + xP[degBF - 2].retBF();

	// final doublings / addition
	/*
	b4_F6 = (yQ + xQ[1](1 + xQ + xpBF^8 + xpBF^4) + xpBF^4xQ + ypBF^4) +
	        (xQ[1] + xpBF^4)w + (xpBF^8 + xpBF^4)w^2 + w^3 + (xQ[1] + xQ)w^4
	*/

	// b4_3
	b4_F6.xF3[0].xBF[2].plusOne();

	// b4_4
	b4_F6.xF3[0].xBF[1] = xQ[1].retBF() + xq0;

	// xpBF := xpBF^4
	BaseField::square(tmpBF_1, xpBF);
	BaseField::square(xpBF, tmpBF_1);

	// b4_1
	b4_F6.xF3[1].xBF[1] = xQ[1].retBF() + tmpBF_1;

	// tmpBF_1 := xpBF^8
	BaseField::square(tmpBF_1, xpBF);

	// b4_2;
	b4_F6.xF3[0].xBF[2] = tmpBF_1 + xpBF;

	// b4_0
	// tmpBF_1 := b4_2 + xq0
	tmpBF_1 = b4_F6.xF3[1].xBF[2] + xq0;
	tmpBF_1.plusOne();
	BaseField::mulCombR2L_RegSize(b4_F6.xF3[0].xBF[0],xQ[1].retBF(),tmpBF_1);

	//tmpBF_1 = xpBF^4xq0
	BaseField::mulCombR2L_RegSize(tmpBF_1, xpBF, xq0);
	b4_F6.xF3[1].xBF[2] += tmpBF_1;
	b4_F6.xF3[1].xBF[0] += yq0;

	// ypBF := ypBF^4
	BaseField::square(tmpBF_1, ypBF);
	BaseField::square(ypBF, tmpBF_1);
	b4_F6.xF3[0].xBF[0] += ypBF;

	// f^4
	F2_ExtField3_2_2<BaseField>::square(tmp3_2_2, pairingValue);
	F2_ExtField3_2_2<BaseField>::square(pairingValue, tmp3_2_2);

	// f^4(y+b4(x))
	F2_ExtField3_2_2<BaseField>::sparseMul(tmp3_2_2, pairingValue, b4_F6);
	pairingValue.copyReg(tmp3_2_2);


	// final exponentiation
	/* !! perform only (degBF+1)/2 squarings !! */

	for (int i = 0; i < (degBF + 1) / 2; ++i) {

		F2_ExtField3_2_2<BaseField>::square(tmp3_2_2, pairingValue);
		pairingValue.copyReg(tmp3_2_2);
	}

	return pairingValue;
}



// use window method for multiplication on fields
template<class BaseField, int wSize>
F2_ExtField6_2<BaseField> etaT_pairing6_2(const BaseField& xp0, const BaseField& xq0, const BaseField& yp0, const BaseField& yq0) {

	static const int degBF = BaseField::ext_degree;
	static const int nFull = F2_ExtField6_2<BaseField>::nFull;

	F2_ExtField6_2<BaseField> pairingValue, alphaBeta, tmp6_2;
	pairingValue.clear();
	pairingValue.plusOne();

	F2_ExtField6<BaseField> alpha, beta, b4_F6;
	alpha.clear();
	beta.clear();
	b4_F6.clear();

	BaseField xpBF, ypBF, tmpBF_1;
	std::vector<AlignArray32<BaseField>> xP(degBF), xQ(degBF), yP(degBF), yQ(degBF);

	int k1, k2, k3, k4, k5, k6;

	// precomputation
	xP[0].store(xp0);
	xQ[0].store(xq0);
	yP[0].store(yp0);
	yQ[0].store(yq0);


	for (int i = 1; i < degBF; ++i) {

		AlignArray32<BaseField>::square(xP[i], xP[i - 1]);
		AlignArray32<BaseField>::square(xQ[i], xQ[i - 1]);
		AlignArray32<BaseField>::square(yP[i], yP[i - 1]);
		AlignArray32<BaseField>::square(yQ[i], yQ[i - 1]);
	}

	// Miller loop
	for (int i = 0; i < (degBF - 3) / 2; ++i) {


		k1 = ((3 * degBF - 9 - 6 * i) / 2) % degBF;
		k2 = (k1 + 1) % degBF;
		k3 = (k2 + 1) % degBF;
		k4 = ((3 * degBF - 3 - 6 * i) / 2) % degBF;
		k5 = (k4 + 1) % degBF;
		k6 = (k5 + 1) % degBF;

		// compute \alpha = (alpha,1)
		// alpha = a0 + a1w + a2w^2 + a4w^4

		// a4 = xP[k4] + xP[k5]
		(xP[k4] + xP[k5]).load(alpha.xBF[4]);

		// a2 = xP[k4] + 1 + xQ[k3]
		(xP[k4] + xQ[k3]).load(alpha.xBF[2]);
		alpha.xBF[2].plusOne();

		// a1 = xQ[k2] + xQ[k3]
		(xQ[k2] + xQ[k3]).load(alpha.xBF[1]);

		//a0 = yQ[k2] + a2xQ[k2] + a4xQ[k3] + yP[k4]
		BaseField::mulCombL2RwithWindow<wSize>(tmpBF_1, alpha.xBF[2], xQ[k2].retBF());
		alpha.xBF[0] = yQ[k2].retBF() + tmpBF_1;
		BaseField::mulCombL2RwithWindow<wSize>(tmpBF_1, alpha.xBF[4], xQ[k3].retBF());
		alpha.xBF[0] += tmpBF_1;
		alpha.xBF[0] += yP[k4].retBF();


		// compute \beta = (beta,1)
		// beta = b0 + b1w + b2w^2 + b4w^4

		// b1 = xP[k5] + xP[k6]
		(xP[k5] + xP[k6]).load(beta.xBF[1]);

		// b2 = xP[k6] + xQ[k1] + 1
		(xP[k6] + xQ[k1]).load(beta.xBF[2]);
		beta.xBF[2].plusOne();

		// b4 = xQ[k1] + xQ[k2] + 1
		(xQ[k1] + xQ[k2]).load(beta.xBF[4]);
		beta.xBF[4].plusOne();

		// b0 = yQ[k1] + b1xQ[k1] + yP[k5] + xP[k6](xP[k5] + xQ[k2]) + xP[k5]
		BaseField::mulCombL2RwithWindow<wSize>(tmpBF_1, beta.xBF[1], xQ[k1].retBF());
		beta.xBF[0] = yQ[k1].retBF() + tmpBF_1;
		beta.xBF[0] += yP[k5].retBF();
		beta.xBF[0] += xP[k5].retBF();
		BaseField::mulCombL2RwithWindow<wSize>( tmpBF_1, xP[k6].retBF(), xP[k5].retBF() + xQ[k2].retBF() );
		beta.xBF[0] += tmpBF_1;

		// \alpha\beta
		F2_ExtField6_2<BaseField>::alpha_beta_withWindow<wSize>(alphaBeta, alpha, beta);

		F2_ExtField6_2<BaseField>::mulCombL2RwithWindow<wSize>(tmp6_2, pairingValue, alphaBeta);
		pairingValue.copyReg(tmp6_2);

	}

	// extract current point (xP,yP)
	xpBF = xP[degBF - 3].retBF();
	xpBF.plusOne();
	ypBF = yP[degBF - 3].retBF() + xP[degBF - 2].retBF();

	// final doublings / addition
	/*
	b4_F6 = (yQ + xQ[1](1 + xQ + xpBF^8 + xpBF^4) + xpBF^4xQ + ypBF^4) +
	        (xQ[1] + xpBF^4)w + (xpBF^8 + xpBF^4)w^2 + w^3 + (xQ[1] + xQ)w^4
	*/

	// b4_3
	b4_F6.xBF[3].plusOne();

	// b4_4
	b4_F6.xBF[4] = xQ[1].retBF() + xq0;

	// xpBF := xpBF^4
	BaseField::square(tmpBF_1, xpBF);
	BaseField::square(xpBF, tmpBF_1);

	// b4_1
	b4_F6.xBF[1] = xQ[1].retBF() + tmpBF_1;

	// tmpBF_1 := xpBF^8
	BaseField::square(tmpBF_1, xpBF);

	// b4_2;
	b4_F6.xBF[2] = tmpBF_1 + xpBF;

	// b4_0
	// tmpBF_1 := b4_2 + xq0
	tmpBF_1 = b4_F6.xBF[2] + xq0;
	tmpBF_1.plusOne();
	BaseField::mulCombL2RwithWindow<wSize>(b4_F6.xBF[0],xQ[1].retBF(),tmpBF_1);

	//tmpBF_1 = xpBF^4xq0
	BaseField::mulCombL2RwithWindow<wSize>(tmpBF_1, xpBF, xq0);
	b4_F6.xBF[0] += tmpBF_1;
	b4_F6.xBF[0] += yq0;

	// ypBF := ypBF^4
	BaseField::square(tmpBF_1, ypBF);
	BaseField::square(ypBF, tmpBF_1);
	b4_F6.xBF[0] += ypBF;

	// f^4
	F2_ExtField6_2<BaseField>::square(tmp6_2, pairingValue);
	F2_ExtField6_2<BaseField>::square(pairingValue, tmp6_2);

	// f^4(y+b4(x))
	F2_ExtField6_2<BaseField>::sparseMul_withWindow<wSize>(tmp6_2, pairingValue, b4_F6);
	pairingValue.copyReg(tmp6_2);


	// final exponentiation
	/* !! perform only (degBF+1)/2 squarings !! */

	for (int i = 0; i < (degBF + 1) / 2; ++i) {

		F2_ExtField6_2<BaseField>::square(tmp6_2, pairingValue);
		pairingValue.copyReg(tmp6_2);
	}

	return pairingValue;
}

/*\gamma_1������*/
template<class BaseField, int wSize>
F2_ExtField3_2_2<BaseField> etaT_pairing3_2_2(const BaseField& xp0, const BaseField& xq0, const BaseField& yp0, const BaseField& yq0) {

	static const int degBF = BaseField::ext_degree;
	static const int nFull = F2_ExtField6_2<BaseField>::nFull;

	F2_ExtField3_2_2<BaseField> pairingValue, alphaBeta, tmp3_2_2;
	pairingValue.clear();
	pairingValue.plusOne();

	F2_ExtField3_2<BaseField> alpha, beta, b4_F6;
	alpha.clear();
	beta.clear();
	b4_F6.clear();

	BaseField xpBF, ypBF, d, tmpBF_1, tmpBF_2;
	std::vector<AlignArray32<BaseField>> xP(degBF), xQ(degBF), yP(degBF), yQ(degBF);

	int k1, k2, k3, k4, k5, k6;

	// precomputation
	xP[0].store(xp0);
	xQ[0].store(xq0);
	yP[0].store(yp0);
	yQ[0].store(yq0);


	for (int i = 1; i < degBF; ++i) {

		AlignArray32<BaseField>::square(xP[i], xP[i - 1]);
		AlignArray32<BaseField>::square(xQ[i], xQ[i - 1]);
		AlignArray32<BaseField>::square(yP[i], yP[i - 1]);
		AlignArray32<BaseField>::square(yQ[i], yQ[i - 1]);
	}

	// Miller loop
	for (int i = 0; i < (degBF - 3) / 2; ++i) {


		k1 = ((3 * degBF - 9 - 6 * i) / 2) % degBF;
		k2 = (k1 + 1) % degBF;
		k3 = (k2 + 1) % degBF;
		k4 = ((3 * degBF - 3 - 6 * i) / 2) % degBF;
		k5 = (k4 + 1) % degBF;
		k6 = (k5 + 1) % degBF;

		// compute \alpha = (alpha,1)
		// alpha = a0 + a1w + a2w^2 + a3s (a3=c)

		// a2 = xP[k4] + xP[k5]
		(xP[k4] + xP[k5]).load(alpha.xF3[0].xBF[2]);

		// a3 = xQ[k2] + xP[k5] + 1
		(xQ[k2] + xP[k5]).load(alpha.xF3[0].xBF[1]);

		// !!! use a3+1 for a0 !!!
		BaseField::mulCombL2RwithWindow<wSize>(tmpBF_1, alpha.xF3[0].xBF[1], xQ[k3].retBF()); //tmpBF_1 = (a3 + 1)xQ[k3]
		alpha.xF3[0].plusOne();

		// a1 = xQ[k3] + xP[k5] + 1 + \gamma_1
		(xP[k5] + xQ[k3]).load(alpha.xF3[0].xBF[1]);
		alpha.xF3[0].xBF[1].plusOne();

		//a0 = yQ[k2] + (a3 + 1)xQ[k3] + (xP[k4] + 1 + xQ[k3])xQ[k2] + yP[k4] + xP[k4] + \gamma_1
		alpha.xF3[0].xBF[0] = yQ[k2].retBF() + tmpBF_1;
		alpha.xF3[0].xBF[0] += xP[k4].retBF();
		alpha.xF3[0].xBF[0] += yP[k4].retBF();
		tmpBF_1 = xP[k4].retBF() + xQ[k3].retBF();
		tmpBF_1.plusOne();
		BaseField::mulCombL2RwithWindow<wSize>(tmpBF_2, tmpBF_1, xQ[k2].retBF());
		alpha.xF3[0].xBF[0] += tmpBF_2;



		// compute \beta = (beta,1)
		// beta = b0 + b1w + b2w^2 + b3s (a3=b3=c)

		// b1 = xQ[k2] + xP[k6] + 1 + \gamma_1
		(xQ[k2] + xP[k6]).load(beta.xF3[0].xBF[1]);
		beta.xF3[0].xBF[1].plusOne();

		// b2 = xQ[k2] + xQ[k1] + \tgamma_1
		(xQ[k2] + xQ[k1]).load(beta.xF3[0].xBF[2]);
	
		// b0 = yQ[k1] + yP[k5] + xQ[k1](xP[k5] + xP[k6] + 1) + a3*xP[k6] + xP[k5]+ \gamma_1
		tmpBF_1 = xP[k6].retBF() + xP[k5].retBF();
		tmpBF_1.plusOne();
		BaseField::mulCombL2RwithWindow<wSize>(beta.xF3[0].xBF[0], tmpBF_1, xQ[k1].retBF());
		beta.xF3[0].xBF[0] += yQ[k1].retBF();
		beta.xF3[0].xBF[0] += yP[k5].retBF();
		beta.xF3[0].xBF[0] += xP[k5].retBF();
		BaseField::mulCombL2RwithWindow<wSize>(tmpBF_1, xP[k6].retBF(), alpha.xF3[1].xBF[0]);		
		beta.xF3[0].xBF[0] += tmpBF_1;



		// \alpha\beta
		F2_ExtField3_2_2<BaseField>::alpha_beta_withWindow<wSize>(alphaBeta, alpha, beta, d);

		F2_ExtField3_2_2<BaseField>::mulCombL2RwithWindow<wSize>(tmp3_2_2, pairingValue, alphaBeta);
		pairingValue.copyReg(tmp3_2_2);

	}

	// extract current point (xP,yP)
	xpBF = xP[degBF - 3].retBF();
	xpBF.plusOne();
	ypBF = yP[degBF - 3].retBF() + xP[degBF - 2].retBF();

	// final doublings / addition
	/*
	b4_F6 = (yQ + xQ[1](1 + xQ + xpBF^8 + xpBF^4) + xpBF^4xQ + ypBF^4) +
	        (xQ[1] + xpBF^4)w + (xpBF^8 + xpBF^4)w^2 + w^3 + (xQ[1] + xQ)w^4
	*/

	// b4_3
	b4_F6.xF3[0].xBF[2].plusOne();

	// b4_4
	b4_F6.xF3[0].xBF[1] = xQ[1].retBF() + xq0;

	// xpBF := xpBF^4
	BaseField::square(tmpBF_1, xpBF);
	BaseField::square(xpBF, tmpBF_1);

	// b4_1
	b4_F6.xF3[1].xBF[1] = xQ[1].retBF() + tmpBF_1;

	// tmpBF_1 := xpBF^8
	BaseField::square(tmpBF_1, xpBF);

	// b4_2;
	b4_F6.xF3[0].xBF[2] = tmpBF_1 + xpBF;

	// b4_0
	// tmpBF_1 := b4_2 + xq0
	tmpBF_1 = b4_F6.xF3[1].xBF[2] + xq0;
	tmpBF_1.plusOne();
	BaseField::mulCombL2RwithWindow<wSize>(b4_F6.xF3[0].xBF[0],xQ[1].retBF(),tmpBF_1);

	//tmpBF_1 = xpBF^4xq0
	BaseField::mulCombL2RwithWindow<wSize>(tmpBF_1, xpBF, xq0);
	b4_F6.xF3[1].xBF[2] += tmpBF_1;
	b4_F6.xF3[1].xBF[0] += yq0;

	// ypBF := ypBF^4
	BaseField::square(tmpBF_1, ypBF);
	BaseField::square(ypBF, tmpBF_1);
	b4_F6.xF3[0].xBF[0] += ypBF;

	// f^4
	F2_ExtField3_2_2<BaseField>::square(tmp3_2_2, pairingValue);
	F2_ExtField3_2_2<BaseField>::square(pairingValue, tmp3_2_2);

	// f^4(y+b4(x))
	F2_ExtField3_2_2<BaseField>::sparseMul_withWindow<wSize>(tmp3_2_2, pairingValue, b4_F6);
	pairingValue.copyReg(tmp3_2_2);


	// final exponentiation
	/* !! perform only (degBF+1)/2 squarings !! */

	for (int i = 0; i < (degBF + 1) / 2; ++i) {

		F2_ExtField3_2_2<BaseField>::square(tmp3_2_2, pairingValue);
		pairingValue.copyReg(tmp3_2_2);
	}

	return pairingValue;
}



// multiplication using load with shift (8-bit size)
template<class BaseField>
F2_ExtField6_2<BaseField> etaT_pairing6_2LoadShift8(const BaseField& xp0, const BaseField& xq0, const BaseField& yp0, const BaseField& yq0) {

	static const int degBF = BaseField::ext_degree;
	static const int nFull = F2_ExtField6_2<BaseField>::nFull;

	F2_ExtField6_2<BaseField> pairingValue, alphaBeta, tmp6_2;
	pairingValue.clear();
	pairingValue.plusOne();

	F2_ExtField6<BaseField> alpha, beta, b4_F6;
	alpha.clear();
	beta.clear();
	b4_F6.clear();

	BaseField xpBF, ypBF, tmpBF_1;
	std::vector<AlignArray32<BaseField>> xP(degBF), xQ(degBF), yP(degBF), yQ(degBF);

	int k1, k2, k3, k4, k5, k6;

	// precomputation
	xP[0].store(xp0);
	xQ[0].store(xq0);
	yP[0].store(yp0);
	yQ[0].store(yq0);

	for (int i = 1; i < degBF; ++i) {

		AlignArray32<BaseField>::square(xP[i], xP[i - 1]);
		AlignArray32<BaseField>::square(xQ[i], xQ[i - 1]);
		AlignArray32<BaseField>::square(yP[i], yP[i - 1]);
		AlignArray32<BaseField>::square(yQ[i], yQ[i - 1]);
	}

	// Miller loop
	for (int i = 0; i < (degBF - 3) / 2; ++i) {


		k1 = ((3 * degBF - 9 - 6 * i) / 2) % degBF;
		k2 = (k1 + 1) % degBF;
		k3 = (k2 + 1) % degBF;
		k4 = ((3 * degBF - 3 - 6 * i) / 2) % degBF;
		k5 = (k4 + 1) % degBF;
		k6 = (k5 + 1) % degBF;

		// compute \alpha = (alpha,1)
		// alpha = a0 + a1w + a2w^2 + a4w^4

		// a4 = xP[k4] + xP[k5]
		(xP[k4] + xP[k5]).load(alpha.xBF[4]);

		// a2 = xP[k4] + 1 + xQ[k3]
		(xP[k4] + xQ[k3]).load(alpha.xBF[2]);
		alpha.xBF[2].plusOne();

		// a1 = xQ[k2] + xQ[k3]
		(xQ[k2] + xQ[k3]).load(alpha.xBF[1]);

		//a0 = yQ[k2] + a2xQ[k2] + a4xQ[k3] + yP[k4]
		BaseField::mulLoadShift8(tmpBF_1, alpha.xBF[2], xQ[k2].retBF());
		alpha.xBF[0] = yQ[k2].retBF() + tmpBF_1;
		BaseField::mulLoadShift8(tmpBF_1, alpha.xBF[4], xQ[k3].retBF());
		alpha.xBF[0] += tmpBF_1;
		alpha.xBF[0] += yP[k4].retBF();


		// compute \beta = (beta,1)
		// beta = b0 + b1w + b2w^2 + b4w^4

		// b1 = xP[k5] + xP[k6]
		(xP[k5] + xP[k6]).load(beta.xBF[1]);

		// b2 = xP[k6] + xQ[k1] + 1
		(xP[k6] + xQ[k1]).load(beta.xBF[2]);
		beta.xBF[2].plusOne();

		// b4 = xQ[k1] + xQ[k2] + 1
		(xQ[k1] + xQ[k2]).load(beta.xBF[4]);
		beta.xBF[4].plusOne();

		// b0 = yQ[k1] + b1xQ[k1] + yP[k5] + xP[k6](xP[k5] + xQ[k2]) + xP[k5]
		BaseField::mulLoadShift8(tmpBF_1, beta.xBF[1], xQ[k1].retBF());
		beta.xBF[0] = yQ[k1].retBF() + tmpBF_1;
		beta.xBF[0] += yP[k5].retBF();
		beta.xBF[0] += xP[k5].retBF();
		BaseField::mulLoadShift8( tmpBF_1, xP[k6].retBF(), xP[k5].retBF() + xQ[k2].retBF() );
		beta.xBF[0] += tmpBF_1;

		// \alpha\beta
		F2_ExtField6_2<BaseField>::alpha_betaLoadShift8(alphaBeta, alpha, beta);

		F2_ExtField6_2<BaseField>::mulLoadShift8(tmp6_2, pairingValue, alphaBeta);
	    pairingValue.copyReg(tmp6_2);

	}

	// extract current point (xP,yP)
	xpBF = xP[degBF - 3].retBF();
	xpBF.plusOne();
	ypBF = yP[degBF - 3].retBF() + xP[degBF - 2].retBF();

	// final doublings / addition
	/*
	b4_F6 = (yQ + xQ[1](1 + xQ + xpBF^8 + xpBF^4) + xpBF^4xQ + ypBF^4) +
	        (xQ[1] + xpBF^4)w + (xpBF^8 + xpBF^4)w^2 + w^3 + (xQ[1] + xQ)w^4
	*/

	// b4_3
	b4_F6.xBF[3].plusOne();

	// b4_4
	b4_F6.xBF[4] = xQ[1].retBF() + xq0;

	// xpBF := xpBF^4
	BaseField::square(tmpBF_1, xpBF);
	BaseField::square(xpBF, tmpBF_1);

	// b4_1
	b4_F6.xBF[1] = xQ[1].retBF() + tmpBF_1;

	// tmpBF_1 := xpBF^8
	BaseField::square(tmpBF_1, xpBF);

	// b4_2;
	b4_F6.xBF[2] = tmpBF_1 + xpBF;

	// b4_0
	// tmpBF_1 := b4_2 + xq0
	tmpBF_1 = b4_F6.xBF[2] + xq0;
	tmpBF_1.plusOne();
	BaseField::mulLoadShift8(b4_F6.xBF[0],xQ[1].retBF(),tmpBF_1);

	//tmpBF_1 = xpBF^4xq0
	BaseField::mulLoadShift8(tmpBF_1, xpBF, xq0);
	b4_F6.xBF[0] += tmpBF_1;
	b4_F6.xBF[0] += yq0;

	// ypBF := ypBF^4
	BaseField::square(tmpBF_1, ypBF);
	BaseField::square(ypBF, tmpBF_1);
	b4_F6.xBF[0] += ypBF;

	// f^4
	F2_ExtField6_2<BaseField>::square(tmp6_2, pairingValue);
	F2_ExtField6_2<BaseField>::square(pairingValue, tmp6_2);

	// f^4(y+b4(x))
	F2_ExtField6_2<BaseField>::sparseMulLoadShift8(tmp6_2, pairingValue, b4_F6);
	pairingValue.copyReg(tmp6_2);


	// final exponentiation
	/* !! perform only (degBF+1)/2 squarings !! */

	for (int i = 0; i < (degBF + 1) / 2; ++i) {

		F2_ExtField6_2<BaseField>::square(tmp6_2, pairingValue);
		pairingValue.copyReg(tmp6_2);
	}

	return pairingValue;
}



// 3-2-2
/*\gamma_1������*/
template<class BaseField>
F2_ExtField3_2_2<BaseField> etaT_pairing3_2_2LoadShift8(const BaseField& xp0, const BaseField& xq0, const BaseField& yp0, const BaseField& yq0) {

	static const int degBF = BaseField::ext_degree;
	static const int nFull = F2_ExtField6_2<BaseField>::nFull;

	F2_ExtField3_2_2<BaseField> pairingValue, alphaBeta, tmp3_2_2;
	pairingValue.clear();
	pairingValue.plusOne();

	F2_ExtField3_2<BaseField> alpha, beta, b4_F6;
	alpha.clear();
	beta.clear();
	b4_F6.clear();

	BaseField xpBF, ypBF, d, tmpBF_1, tmpBF_2;
	std::vector<AlignArray32<BaseField>> xP(degBF), xQ(degBF), yP(degBF), yQ(degBF);

	int k1, k2, k3, k4, k5, k6;

	// precomputation
	xP[0].store(xp0);
	xQ[0].store(xq0);
	yP[0].store(yp0);
	yQ[0].store(yq0);


	for (int i = 1; i < degBF; ++i) {

		AlignArray32<BaseField>::square(xP[i], xP[i - 1]);
		AlignArray32<BaseField>::square(xQ[i], xQ[i - 1]);
		AlignArray32<BaseField>::square(yP[i], yP[i - 1]);
		AlignArray32<BaseField>::square(yQ[i], yQ[i - 1]);
	}

	// Miller loop
	for (int i = 0; i < (degBF - 3) / 2; ++i) {


		k1 = ((3 * degBF - 9 - 6 * i) / 2) % degBF;
		k2 = (k1 + 1) % degBF;
		k3 = (k2 + 1) % degBF;
		k4 = ((3 * degBF - 3 - 6 * i) / 2) % degBF;
		k5 = (k4 + 1) % degBF;
		k6 = (k5 + 1) % degBF;

		// compute \alpha = (alpha,1)
		// alpha = a0 + a1w + a2w^2 + a3s (a3=c)

		// a2 = xP[k4] + xP[k5]
		(xP[k4] + xP[k5]).load(alpha.xF3[0].xBF[2]);

		// a3 = xQ[k2] + xP[k5] + 1
		(xQ[k2] + xP[k5]).load(alpha.xF3[0].xBF[1]);

		// !!! use a3+1 for a0 !!!
		BaseField::mulLoadShift8(tmpBF_1, alpha.xF3[0].xBF[1], xQ[k3].retBF()); //tmpBF_1 = (a3 + 1)xQ[k3]
		alpha.xF3[0].plusOne();

		// a1 = xQ[k3] + xP[k5] + 1 + \gamma_1
		(xP[k5] + xQ[k3]).load(alpha.xF3[0].xBF[1]);
		alpha.xF3[0].xBF[1].plusOne();

		//a0 = yQ[k2] + (a3 + 1)xQ[k3] + (xP[k4] + 1 + xQ[k3])xQ[k2] + yP[k4] + xP[k4] + \gamma_1
		alpha.xF3[0].xBF[0] = yQ[k2].retBF() + tmpBF_1;
		alpha.xF3[0].xBF[0] += xP[k4].retBF();
		alpha.xF3[0].xBF[0] += yP[k4].retBF();
		tmpBF_1 = xP[k4].retBF() + xQ[k3].retBF();
		tmpBF_1.plusOne();
		BaseField::mulLoadShift8(tmpBF_2, tmpBF_1, xQ[k2].retBF());
		alpha.xF3[0].xBF[0] += tmpBF_2;



		// compute \beta = (beta,1)
		// beta = b0 + b1w + b2w^2 + b3s (a3=b3=c)

		// b1 = xQ[k2] + xP[k6] + 1 + \gamma_1
		(xQ[k2] + xP[k6]).load(beta.xF3[0].xBF[1]);
		beta.xF3[0].xBF[1].plusOne();

		// b2 = xQ[k2] + xQ[k1] + \tgamma_1
		(xQ[k2] + xQ[k1]).load(beta.xF3[0].xBF[2]);
	
		// b0 = yQ[k1] + yP[k5] + xQ[k1](xP[k5] + xP[k6] + 1) + a3*xP[k6] + xP[k5]+ \gamma_1
		tmpBF_1 = xP[k6].retBF() + xP[k5].retBF();
		tmpBF_1.plusOne();
		BaseField::mulLoadShift8(beta.xF3[0].xBF[0], tmpBF_1, xQ[k1].retBF());
		beta.xF3[0].xBF[0] += yQ[k1].retBF();
		beta.xF3[0].xBF[0] += yP[k5].retBF();
		beta.xF3[0].xBF[0] += xP[k5].retBF();
		BaseField::mulLoadShift8(tmpBF_1, xP[k6].retBF(), alpha.xF3[1].xBF[0]);		
		beta.xF3[0].xBF[0] += tmpBF_1;



		// \alpha\beta
		F2_ExtField3_2_2<BaseField>::alpha_betaLoadShift8(alphaBeta, alpha, beta, d);

		F2_ExtField3_2_2<BaseField>::mulLoadShift8(tmp3_2_2, pairingValue, alphaBeta);
		pairingValue.copyReg(tmp3_2_2);

	}

	// extract current point (xP,yP)
	xpBF = xP[degBF - 3].retBF();
	xpBF.plusOne();
	ypBF = yP[degBF - 3].retBF() + xP[degBF - 2].retBF();

	// final doublings / addition
	/*
	b4_F6 = (yQ + xQ[1](1 + xQ + xpBF^8 + xpBF^4) + xpBF^4xQ + ypBF^4) +
	        (xQ[1] + xpBF^4)w + (xpBF^8 + xpBF^4)w^2 + w^3 + (xQ[1] + xQ)w^4
	*/

	// b4_3
	b4_F6.xF3[0].xBF[2].plusOne();

	// b4_4
	b4_F6.xF3[0].xBF[1] = xQ[1].retBF() + xq0;

	// xpBF := xpBF^4
	BaseField::square(tmpBF_1, xpBF);
	BaseField::square(xpBF, tmpBF_1);

	// b4_1
	b4_F6.xF3[1].xBF[1] = xQ[1].retBF() + tmpBF_1;

	// tmpBF_1 := xpBF^8
	BaseField::square(tmpBF_1, xpBF);

	// b4_2;
	b4_F6.xF3[0].xBF[2] = tmpBF_1 + xpBF;

	// b4_0
	// tmpBF_1 := b4_2 + xq0
	tmpBF_1 = b4_F6.xF3[1].xBF[2] + xq0;
	tmpBF_1.plusOne();
	BaseField::mulLoadShift8(b4_F6.xF3[0].xBF[0],xQ[1].retBF(),tmpBF_1);

	//tmpBF_1 = xpBF^4xq0
	BaseField::mulLoadShift8(tmpBF_1, xpBF, xq0);
	b4_F6.xF3[1].xBF[2] += tmpBF_1;
	b4_F6.xF3[1].xBF[0] += yq0;

	// ypBF := ypBF^4
	BaseField::square(tmpBF_1, ypBF);
	BaseField::square(ypBF, tmpBF_1);
	b4_F6.xF3[0].xBF[0] += ypBF;

	// f^4
	F2_ExtField3_2_2<BaseField>::square(tmp3_2_2, pairingValue);
	F2_ExtField3_2_2<BaseField>::square(pairingValue, tmp3_2_2);

	// f^4(y+b4(x))
	F2_ExtField3_2_2<BaseField>::sparseMulLoadShift8(tmp3_2_2, pairingValue, b4_F6);
	pairingValue.copyReg(tmp3_2_2);


	// final exponentiation
	/* !! perform only (degBF+1)/2 squarings !! */

	for (int i = 0; i < (degBF + 1) / 2; ++i) {

		F2_ExtField3_2_2<BaseField>::square(tmp3_2_2, pairingValue);
		pairingValue.copyReg(tmp3_2_2);
	}

	return pairingValue;
}
