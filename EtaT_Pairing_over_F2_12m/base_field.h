#pragma once

#include <emmintrin.h>
#include "char2_128.h"

/*
base field: $\mathbb F_{2^m}$,
*/

// base field
template<class T, int m> class F2_BaseField {

public:

	static const int ext_degree = m;
	static const int n = (m + 31) / 32; /* 32 bit分割 */
	static const int nR = (m + T::regLength - 1) / T::regLength; /* 128 or 256 bit分割 */
	static const int nFull = nR * T::regLength / 32;
	static const int stride32 = T::regLength / 32; /* for store data to uint32_t array */
	static const int regLength = T::regLength;

	//__declspec(align(16)) uint32_t x32[n];
	T xReg[nR];



	F2_BaseField() {}

#if 0
	__forceinline void clear32() {

		for (unsigned int i = 0; i < n; ++i) this->x32[i] = 0;
	}
#endif

	__forceinline void clearReg() {

		for (unsigned int i = 0; i < nR; ++i) this->xReg[i].clear();
	}

	__forceinline void clear() {

		//clear32();
		clearReg();
	}

	__forceinline void setString(const char *str) {

		clear();

		std::string noSpStr;
		removeSpace(noSpStr, str);

		int len = noSpStr.length();
		int position = len -1;
		int nStrLen = (len + T::regLength - 1) / T::regLength;
		int remainderNum = len - (nStrLen - 1) * T::regLength;

		if (nStrLen == 1) {

			xReg[0].setStringNoChk(noSpStr.c_str());
			return;
		}
		else {

			xReg[nStrLen - 1].setStringNoChk( noSpStr.substr(0,remainderNum).c_str() );
			for (int i = 0; i <= nStrLen - 2; ++i) {

				xReg[nStrLen - i - 2].setStringNoChk(noSpStr.substr( remainderNum + i*T::regLength, T::regLength ).c_str());
			}
		}

	}

	__forceinline void setBit1(int position) {

		assert(0 <= position && position < ext_degree);
		int posReg = position / regLength;
		int posRemainder = position - regLength * posReg;

		this->xReg[posReg].setBit1(posRemainder);
	}

	__forceinline void setRandom() {

		for (unsigned int i = 0; i < nR; ++i) this->xReg[i].setRandom();
		clearUpDeg();
	}

#if 0
	__forceinline void copyTo32() {

		for (int i = 0; i < nR; ++i) xReg[i].store(x32 + i*stride32);
	}
#endif

	__forceinline void copyTo32(uint32_t *a) const {

		for (int i = 0; i < nR; ++i) xReg[i].store(a + i*stride32);
	}

	/* !!! regLength = 128決め打ちの状態になっているので注意 !!! */
#if 0
	__forceinline void loadFrom32() {

		uint32_t tmp32[stride32];
		for (int i = 0; i < nR - 1; ++i) xReg[i].set32(x32[i * stride32], x32[i * stride32 + 1], x32[i * stride32 + 2], x32[i * stride32 + 3]);

		for (int i = 0; i <= (n - 1) % stride32; ++i) tmp32[i] = x32[(nR - 1) * stride32 + i];
		for (int i = (n - 1) % stride32 + 1; i < 4 ; ++i) tmp32[i] = 0;
		xReg[nR - 1].set32(tmp32[0], tmp32[1], tmp32[2], tmp32[3]);
	}
#endif
	// aの配列長はnFull = nR * regLength / 32を仮定
	__forceinline void loadFrom32(const uint32_t *a) {

		for (int i = 0; i < nR; ++i) xReg[i].set32(a[i * stride32], a[i * stride32 + 1], a[i * stride32 + 2], a[i * stride32 + 3]);
	}

	__forceinline void copyReg(const F2_BaseField& a) {

		//for (int i = 0; i < nR; ++i) this->xReg[i] = a.xReg[i];
		for (int i = 0; i < nR; ++i) this->xReg[i].x = a.xReg[i].x;
	}

	std::string getString() const {

		std::string str;
		for (int i = nR - 1; i >= 0; --i) str += xReg[i].getStringAll();

		formatStr(str);
		return str;
	}

	// i番目の文字がposition iのbitに対応
	// formatしない
	std::string getString_R() const {

		std::string str;
		for (int i = 0; i < nR; ++i) str += xReg[i].getStringAll_R();

		return str;
	}

	__forceinline bool testZero() const {

		bool result = true;
		for (unsigned int i = 0; i < nR; ++i) result &= xReg[i].testZero();
		return result;
	}

	__forceinline void plusOne() {

		const char2_128 One128(1, 0, 0, 0);
		xReg[0] ^= One128;

	}

	__forceinline friend F2_BaseField operator&(const F2_BaseField& a, const F2_BaseField& b) {

		F2_BaseField c;
		for (unsigned int i = 0; i < nR; ++i) c.xReg[i] = a.xReg[i] & b.xReg[i];
		return c;
	}

	__forceinline friend F2_BaseField operator|(const F2_BaseField& a, const F2_BaseField& b) {

		F2_BaseField c;
		for (unsigned int i = 0; i < nR; ++i) c.xReg[i] = a.xReg[i] | b.xReg[i];
		return c;
	}

	__forceinline friend F2_BaseField operator^(const F2_BaseField& a, const F2_BaseField& b) {

		F2_BaseField c;
		for (unsigned int i = 0; i < nR; ++i) c.xReg[i] = a.xReg[i] ^ b.xReg[i];
		return c;
	}

	__forceinline friend F2_BaseField operator+(const F2_BaseField& a, const F2_BaseField& b) {

		return a ^ b;
	}

	__forceinline friend F2_BaseField operator-(const F2_BaseField& a, const F2_BaseField& b) {

		return a ^ b;
	}

	__forceinline friend bool operator==(const F2_BaseField& a, const F2_BaseField& b) {

		bool result = true;
		for (unsigned int i = 0; i < nR; ++i) result &= (a.xReg[i] == b.xReg[i]);
		return result;
	}

	__forceinline F2_BaseField operator&=(const F2_BaseField& b) {

		for (unsigned int i = 0; i < nR; ++i) xReg[i] &= b.xReg[i];
		return *this;
	}

	__forceinline F2_BaseField operator|=(const F2_BaseField& b) {

		for (unsigned int i = 0; i < nR; ++i) xReg[i] |= b.xReg[i];
		return *this;
	}

	__forceinline F2_BaseField operator^=(const F2_BaseField& b) {

		for (unsigned int i = 0; i < nR; ++i) xReg[i] ^= b.xReg[i];
		return *this;
	}

	__forceinline F2_BaseField operator+=(const F2_BaseField& b) {

		return *this ^= b;
	}

	__forceinline F2_BaseField operator-=(const F2_BaseField& b) {

		return *this ^= b;
	}

	/*
	拡大次数より大きな位置にあるビットを0にする
	*/
	void clearUpDeg() {

		F2_BaseField tmpF;
		tmpF.clear();

		//uint32_t mask32[1];
		uint32_t tmp32[nFull];
		for (int i = 0; i < nFull; ++i) tmp32[i] = 0;

		//int rem32 = ext_degree % 32;
		int rem32 = ext_degree - 32 * (n - 1);
		std::string str;

		if (rem32 == 0) {

			for (int i = 0; i < n; ++i) tmp32[i] = 0xffffffff;
			tmpF.loadFrom32(tmp32);
			*this &= tmpF;

			return;
		}


		/* stringを極力使わないように修正すべき (uint32_t変数をshiftしてmask) */
#if 1

		for (int i = 0; i < n; ++i) tmp32[i] = 0xffffffff;
		tmp32[n-1] = tmp32[n-1] >> 32 - rem32;
		tmpF.loadFrom32(tmp32);
		*this &= tmpF;

#else
		for (int i = 0; i < 32 - rem32; ++i) str += '0';
		for (int i = 0; i < rem32; ++i) str += '1';

		for (int i = 0; i < n - 1; ++i) tmp32[i] = 0xffffffff;
		setString32NoChk<1>(mask32, str.c_str());
		tmp32[n - 1] = mask32[0];

		tmpF.loadFrom32(tmp32);
		*this &= tmpF;
#endif
	}

	// 1 bit shift
	void shiftLeftBit1() {

		//uint32_t bit;
		//const char2_128 One128(1, 0, 0, 0);

		//for (int i = nR - 1; i > 0; --i) {

		//	this->xReg[i] = T::shiftLeftBit<1>(this->xReg[i]);
		//	bit = this->xReg[i - 1].getBit(127);
		//	if (bit == 1) this->xReg[i] ^= One128;
		//}
		//this->xReg[0] = T::shiftLeftBit<1>(this->xReg[0]);
#if 1
		for (int i = nR - 1; i > 0; --i) this->xReg[i] = T::shiftLeftBit<1>(this->xReg[i]) | T::shiftRightBit<T::regLength - 1>(this->xReg[i - 1]);
		this->xReg[0] =  T::shiftLeftBit<1>(this->xReg[0]);
#else
		for (int i = nR - 1; i > 0; --i) this->xReg[i] = T::shiftLeftBit1(this->xReg[i]) | T::shiftRightBitR_1(this->xReg[i - 1]);
		this->xReg[0] =  T::shiftLeftBit1(this->xReg[0]);

#endif
	}


	static F2_BaseField shiftLeftBit1(const F2_BaseField& a) {

		F2_BaseField c = a;

		//c = a;
		c.shiftLeftBit1();

		return c;
	}

	static void shiftLeftBit1(F2_BaseField& c, const F2_BaseField& a) {

		c.copyReg(a);
		c.shiftLeftBit1();
	}

	void shiftRightBit1() {

		//uint32_t bit;
		//const char2_128 Top1Bit128(0, 0, 0, 0x80000000);

		//for (int i = 0; i < nR - 1; ++i) {

		//	this->xReg[i] = T::shiftRightBit<1>(this->xReg[i]);
		//	bit = this->xReg[i + 1].getBit(0);
		//	if (bit == 1) this->xReg[i] ^= Top1Bit128;
		//}
		//this->xReg[nR - 1] = T::shiftRightBit<1>(this->xReg[nR - 1]);
#if 1
		for (int i = 0; i < nR - 1; ++i) this->xReg[i] = T::shiftRightBit<1>(this->xReg[i]) | T::shiftLeftBit<T::regLength - 1>(this->xReg[i + 1]);
		this->xReg[nR - 1] = T::shiftRightBit<1>(this->xReg[nR - 1]);
#else
		for (int i = 0; i < nR - 1; ++i) this->xReg[i] = T::shiftRightBit1(this->xReg[i]) | T::shiftLeftBitR_1(this->xReg[i + 1]);
		this->xReg[nR - 1] = T::shiftRightBit1(this->xReg[nR - 1]);

#endif
	}

	static F2_BaseField shiftRightBit1(const F2_BaseField& a) {

		F2_BaseField c = a;

		//c = a;
		c.shiftRightBit1();

		return c;
	}

	static void shiftRightBit1(F2_BaseField& c, const F2_BaseField& a) {

		c.copyReg(a);
		c.shiftRightBit1();
	}



	static void shiftLeftBit1Arr8(uint8_t *a) {

		char2_128 tmp1, tmp2;
		for (int i = nR - 1; i > 0; --i) {

			tmp1.x = _mm_loadu_si128((const __m128i*)(a + i * 16));
			tmp2.x = _mm_loadu_si128((const __m128i*)(a + (i - 1) * 16));
			_mm_storeu_si128((__m128i*)(a + i * 16), (T::shiftLeftBit<1>(tmp1) | T::shiftRightBit<T::regLength - 1>(tmp2)).x);
		}

		_mm_storeu_si128((__m128i*)a, T::shiftLeftBit<1>(tmp2).x);
	}

	static void shiftRightBit1Arr8(uint8_t *a) {

		char2_128 tmp1, tmp2;
		for (int i = 0; i < nR - 1; ++i) {

			tmp1.x = _mm_loadu_si128((const __m128i*)(a + i * 16));
			tmp2.x = _mm_loadu_si128((const __m128i*)(a + (i + 1) * 16));
			_mm_storeu_si128((__m128i*)(a + i * 16), (T::shiftRightBit<1>(tmp1) | T::shiftLeftBit<T::regLength - 1>(tmp2)).x);
		}

		_mm_storeu_si128((__m128i*)(a + (nR - 1) * 16), T::shiftRightBit<1>(tmp2).x);
	}

	template<int N> void shiftLeftBit() {

		static const int nRegFloor = N / T::regLength;
		//static const int remainder = N % T::regLength;
		static const int remainder = N - nRegFloor * T::regLength;

		if (N == 0) return;
		if (nRegFloor >= nR) {

			this->clear();
			return;
		}

		// floor(N/regLength) 分移動
		for (int i = 0; i < nR - nRegFloor; ++i) this->xReg[nR - 1 - i] = this->xReg[nR - 1 - nRegFloor - i];
		for (int i = 0; i < nRegFloor; ++i) this->xReg[i].clear();
		if (remainder == 0) return;

		// N % regLength 分移動
		for (int i = nR - 1; i > nRegFloor; --i) this->xReg[i] = T::shiftLeftBit<remainder>(this->xReg[i]) | T::shiftRightBit<T::regLength - remainder>(this->xReg[i - 1]);

		this->xReg[nRegFloor] =  T::shiftLeftBit<remainder>(this->xReg[nRegFloor]);
	}

	template<int N> void shiftRightBit() {

		static const int nRegFloor = N / T::regLength;
		//static const int remainder = N % T::regLength;
		static const int remainder = N - nRegFloor * T::regLength;

		if (N == 0) return;
		if (nRegFloor >= nR) {

			this->clear();
			return;
		}

		// floor(N/regLength) 分移動
		for (int i = 0; i < nR - nRegFloor; ++i) this->xReg[i] = this->xReg[nRegFloor + i];
		for (int i = nR - nRegFloor; i < nR; ++i) this->xReg[i].clear();
		if (remainder == 0) return;

		// N % regLength 分移動
		for (int i = 0; i < nR - nRegFloor - 1; ++i) this->xReg[i] = T::shiftRightBit<remainder>(this->xReg[i]) | T::shiftLeftBit<T::regLength - remainder>(this->xReg[i + 1]);

		this->xReg[nR - nRegFloor - 1] =  T::shiftRightBit<remainder>(this->xReg[nR - nRegFloor - 1]);
	}

	template<int N>
	static F2_BaseField shiftLeftBit(const F2_BaseField& a) {

		F2_BaseField c;
		static const int nRegFloor = N / T::regLength;
		//static const int remainder = N % T::regLength;
		static const int remainder = N - nRegFloor * T::regLength;

		if (N == 0) {
			c.copyReg(a);
			return c;
		}
		if (nRegFloor >= nR) {

			c.clear();
			return c;
		}

		// floor(N/regLength) 分移動
		for (int i = 0; i < nR - nRegFloor; ++i) c.xReg[nR - 1 - i] = a.xReg[nR - 1 - nRegFloor - i];
		for (int i = 0; i < nRegFloor; ++i) c.xReg[i].clear();
		if (remainder == 0) return c;

		// N % regLength 分移動
		for (int i = nR - 1; i > nRegFloor; --i) c.xReg[i] = T::shiftLeftBit<remainder>(c.xReg[i]) | T::shiftRightBit<T::regLength - remainder>(c.xReg[i - 1]);

		c.xReg[nRegFloor] = T::shiftLeftBit<remainder>(c.xReg[nRegFloor]);
		return c;
	}

	template<int N>
	static F2_BaseField shiftRightBit(const F2_BaseField& a) {

		F2_BaseField c;
		static const int nRegFloor = N / T::regLength;
		//static const int remainder = N % T::regLength;
		static const int remainder = N - nRegFloor * T::regLength;

		if (N == 0) {
			c.copyReg(a);
			return c;
		}
		if (nRegFloor >= nR) {

			c.clear();
			return c;
		}

		// floor(N/regLength) 分移動
		for (int i = 0; i < nR - nRegFloor; ++i) c.xReg[i] = a.xReg[nRegFloor + i];
		for (int i = nR - nRegFloor; i < nR; ++i) c.xReg[i].clear();
		if (remainder == 0) return c;

		// N % regLength 分移動
		for (int i = 0; i < nR - nRegFloor - 1; ++i) c.xReg[i] = T::shiftRightBit<remainder>(c.xReg[i]) | T::shiftLeftBit<T::regLength - remainder>(c.xReg[i + 1]);

		c.xReg[nR - nRegFloor - 1] =  T::shiftRightBit<remainder>(c.xReg[nR - nRegFloor - 1]);
		return c;
	}

	template<int N>
	static void shiftLeftBit(F2_BaseField &c, const F2_BaseField& a) {

		c.copyReg(a);
		c.shiftLeftBit<N>();
	}

	template<int N>
	static void shiftRightBit(F2_BaseField &c, const F2_BaseField& a) {

		c.copyReg(a);
		c.shiftRightBit<N>();
	}

	// nB bytes shift
	template<int nB> void shiftLeftByte() {

		static const int N = 8 * nB;
		static const int nRegFloor = N / T::regLength;
		//static const int remainder = N % T::regLength;
		static const int remainder = N - nRegFloor * T::regLength;
		static const int remB = remainder / 8;

		if (N == 0) return;
		if (nRegFloor >= nR) {

			this->clear();
			return;
		}

		// floor(N/regLength) 分移動
		for (int i = 0; i < nR - nRegFloor; ++i) this->xReg[nR - 1 - i] = this->xReg[nR - 1 - nRegFloor - i];
		for (int i = 0; i < nRegFloor; ++i) this->xReg[i].clear();
		if (remainder == 0) return;

		// (N % regLength)/8 byte分移動
		for (int i = nR - 1; i > nRegFloor; --i) this->xReg[i] = T::shiftLeftByte<remB>(this->xReg[i]) | T::shiftRightByte<T::regLength/8 - remB>(this->xReg[i - 1]);

		this->xReg[nRegFloor] =  T::shiftLeftByte<remB>(this->xReg[nRegFloor]);
	}

	template<int nB> void shiftRightByte() {

		static const int N = 8 * nB;
		static const int nRegFloor = N / T::regLength;
		//static const int remainder = N % T::regLength;
		static const int remainder = N - nRegFloor * T::regLength;
		static const int remB = remainder / 8;

		if (N == 0) return;
		if (nRegFloor >= nR) {

			this->clear();
			return;
		}

		// floor(N/regLength) 分移動
		for (int i = 0; i < nR - nRegFloor; ++i) this->xReg[i] = this->xReg[nRegFloor + i];
		for (int i = nR - nRegFloor; i < nR; ++i) this->xReg[i].clear();
		if (remainder == 0) return;

		// (N % regLength)/8 byte分移動
		for (int i = 0; i < nR - nRegFloor - 1; ++i) this->xReg[i] = T::shiftRightByte<remB>(this->xReg[i]) | T::shiftLeftByte<T::regLength/8 - remB>(this->xReg[i + 1]);

		this->xReg[nR - nRegFloor - 1] =  T::shiftRightByte<remB>(this->xReg[nR - nRegFloor - 1]);
	}

	template<int nB>
	static F2_BaseField shiftLeftByte(const F2_BaseField& a) {

		F2_BaseField c;

		c.copyReg(a);
		return c.shiftLeftByte<nB>();
	}

	template<int nB>
	static F2_BaseField shiftRightByte(const F2_BaseField& a) {

		F2_BaseField c;

		c.copyReg(a);
		return c.shiftRightByte<nB>();
	}

	template<int nB>
	static void shiftLeftByte(F2_BaseField& c, const F2_BaseField& a) {

		c.copyReg(a);
		c.shiftLeftByte<nB>();
	}

	template<int nB>
	static void shiftRightByte(F2_BaseField& c, const F2_BaseField& a) {

		c.copyReg(a);
		c.shiftRightByte<nB>();
	}

	void shiftLeftRegSize(int N) {

		if (N >= nR) {

			this->clear();
			return;
		}

		for (int i = 0; i < nR - N; ++i) this->xReg[nR - 1 - i].x = this->xReg[nR - 1 - N - i].x;
		for (int i = 0; i < N; ++i) this->xReg[i].clear();
	}

	void shiftRightRegSize(int N) {

		if (N >= nR) {

			this->clear();
			return;
		}

		for (int i = 0; i < nR - N; ++i) this->xReg[i].x = this->xReg[N + i].x;
		for (int i = nR - N; i < nR; ++i) this->xReg[i].clear();
	}

	static F2_BaseField shiftLeftRegSize(const F2_BaseField& a, int N) {

		F2_BaseField c;

		if (N >= nR) {

			c.clear();
			return c;
		}

		for (int i = 0; i < nR - N; ++i) c.xReg[nR - 1 - i] = a.xReg[nR - 1 - N - i];
		for (int i = 0; i < N; ++i) c.xReg[i].clear();

		return c;
	}

	static F2_BaseField shiftRightRegSize(const F2_BaseField& a, int N) {

		F2_BaseField c;

		if (N >= nR) {

			c.clear();
			return c;
		}

		for (int i = 0; i < nR - N; ++i) c.xReg[i] = a.xReg[N + i];
		for (int i = nR - N; i < nR; ++i) c.xReg[i].clear();

		return c;
	}

	static void shiftLeftRegSize(F2_BaseField& c, const F2_BaseField& a, int N) {

		c.copyReg(a);
		//c = a;
		c.shiftLeftRegSize(N);
	}

	static void shiftRightRegSize(F2_BaseField& c, const F2_BaseField& a, int N) {

		c.copyReg(a);
		//c = a;
		c.shiftRightRegSize(N);
	}

	static void add(F2_BaseField& c, const F2_BaseField& a, const F2_BaseField& b) {
		c = a ^ b;
	}

	static void mulShAdd(F2_BaseField& c, const F2_BaseField& a, const F2_BaseField& b);
	static void mulCombR2L_RegSize(F2_BaseField& c, const F2_BaseField& a, const F2_BaseField& b);
	template<int W> static void mulCombR2L(F2_BaseField& c, const F2_BaseField& a, const F2_BaseField& b);

	template<int M> static void mulMonomial(F2_BaseField& c, const F2_BaseField& a);

	static void square(F2_BaseField& c, const F2_BaseField& a);

	template<int wSize> static void precomputeTable(F2_BaseField *table, const F2_BaseField& a);
	template<int wSize> static void mulCombL2RwithWindow(F2_BaseField& c, const F2_BaseField& a, const F2_BaseField& b);

	static void mulLoadShift8(F2_BaseField& c, const F2_BaseField& a, const F2_BaseField& b);
	static void mulLoadShift64(F2_BaseField& c, const F2_BaseField& a, const F2_BaseField& b);
};



/*
基礎体において，多項式の乗算結果を表す型
*/
template<class T> class F2_BaseField_Double {

	static const int ext_degree_base = T::ext_degree;
	static const int data_length = 2 * T::ext_degree - 1;
	static const int n = T::n; /* 32 bit分割 */
	static const int nR = T::nR; /* 128 or 256 bit分割 */
	static const int nFull = T::nFull;
	static const int stride32 = T::stride32; /* for store data to uint32_t array */

public:

	//__declspec(align(16)) uint32_t x32[n];
	T low, high;



	F2_BaseField_Double() {}

	void clear() {

		low.clear();
		high.clear();
	}

	void clearUpDeg() {

		low.clearUpDeg();
		high.clearUpDeg();
	}

	void reduction(T& c);
};



template<class BaseField> class AlignArray32 {

public:

	static const int ext_degree = BaseField::ext_degree;
	static const int n32 = BaseField::n; /* 32 bit分割 */
	static const int nR = BaseField::nR; /* 128 or 256 bit分割 */
	static const int nFull = BaseField::nFull;
	static const int stride32 = BaseField::stride32; /* for store data to uint32_t array */
	static const int regLength = BaseField::regLength;

	__declspec(align(16)) uint32_t x32[nFull];



	__forceinline void clear() {

		for (int i = 0; i < nFull; ++i) x32[i] = 0;
	}

	__forceinline void load(BaseField& a) const {

		for (int i = 0; i < nR; ++i) a.xReg[i].load(x32 + i * stride32);
	}

	__forceinline void store(const BaseField& a) {

		for (int i = 0; i < nR; ++i) a.xReg[i].store(x32 + i * stride32);
	}

	__forceinline BaseField retBF() const {

		BaseField ret;
		load(ret);
		return ret;
	}




	__forceinline friend AlignArray32 operator+(const AlignArray32& a, const AlignArray32& b) {

		AlignArray32 c;
		BaseField tmp_a, tmp_b, tmp_c;
		a.load(tmp_a);
		b.load(tmp_b);

		tmp_c = tmp_a + tmp_b;

		c.store(tmp_c);
		return c;
	}

	__forceinline friend AlignArray32 operator-(const AlignArray32& a, const AlignArray32& b) {

		AlignArray32 c;
		BaseField tmp_a, tmp_b, tmp_c;
		a.load(tmp_a);
		b.load(tmp_b);

		tmp_c = tmp_a + tmp_b;

		c.store(tmp_c);
		return c;
	}

	static void mulCombR2L_RegSize(AlignArray32& c, const AlignArray32& a, const AlignArray32& b) {

		BaseField tmp_a, tmp_b, tmp_c;
		a.load(tmp_a);
		b.load(tmp_b);

		BaseField::mulCombR2L_RegSize(tmp_c, tmp_a, tmp_b);

		c.store(tmp_c);
	}

	static void square(AlignArray32& c, const AlignArray32& a) {

		BaseField tmp_a, tmp_c;
		a.load(tmp_a);

		BaseField::square(tmp_c, tmp_a);

		c.store(tmp_c);
	}
};



/*
中間体，拡大体において，多項式の乗算結果を表す型
*/
template<class T> class F2_ExtField_Double {

	static const int ext_degree = T::ext_degree;
	static const int base_degree = T::base_degree;
	static const int regLength = T::regLength;

public:

	T low, high;



	F2_ExtField_Double() {}

	void clear() {

		low.clear();
		high.clear();
	}
};
